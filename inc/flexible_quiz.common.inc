<?php
/**
 * @file
 * Common function to flexible_quiz module.
 */

/**
 * Get replaced question url. Replace % by quiz nid.
 */
function flexible_quiz_get_question_url($flexible_quiz_node) {
  $url = variable_get('flexible_quiz_questions_page_url', 'flexible_quiz/questions/%');
  $url = str_replace('%', $flexible_quiz_node, $url);
  return $url;
}

/**
 * Get replaced result url. Replace % by quiz nid.
 */
function flexible_quiz_get_result_url($flexible_quiz_node) {
  $url = variable_get('flexible_quiz_result_page_url', 'flexible_quiz/result/%');
  $url = str_replace('%', $flexible_quiz_node, $url);
  return $url;
}
