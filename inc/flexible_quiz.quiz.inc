<?php
/**
 * @file
 * Functions to make flexible quiz flow.
 */

/**
 * Load Quiz Landing Page.
 */
function flexible_quiz_landing($flexible_quiz_node) {
  _flexible_quiz_add_js_and_css();
  $node = node_load($flexible_quiz_node);
  module_load_include('inc', 'flexible_quiz', 'inc/flexible_quiz.common');
  $node->start_quiz_link = flexible_quiz_get_question_url($flexible_quiz_node);
  return theme('flexible_quiz_landing', array('node' => $node));
}

/**
 * Implements hook_form().
 */
function flexible_quiz_question_form($form, &$form_state, $quiz_nid) {
  _flexible_quiz_add_js_and_css();
  $quiz_questions = _flexible_quiz_questions($quiz_nid);
  $count = count($quiz_questions);

  $form = array();

  $form['quiz']['quiz_nid'] = array(
    '#type' => 'hidden',
    '#id' => 'quiz_nid',
    '#default_value' => $quiz_nid,
  );

  foreach ($quiz_questions as $key => $question) {

    $questions_container_name = 'questions_fieldset_' . $question->nid;

    $background = field_get_items('node', $question, 'field_bkg_image');
    $background_file = file_create_url($background[0]['uri']);

    $prefix = '<div class="question-wrapper  question-'
      . ($key + 1) . '" style="background: url(' . $background_file
      . ') repeat-x;">';

    $form['quiz'][$questions_container_name]['#prefix'] = $prefix;

    $question_name = 'question_' . $question->nid;

    $question_number = $key + 1;

    $question_markup = '<div class="question-number">' . t('Question') . ' '
      . $question_number . ' ' . t('of') . ' ' . $count . '</div>';

    $form['quiz'][$questions_container_name]['intro_question'] = array(
      '#markup' => $question_markup,
      '#weight' => 1,
    );

    $form['quiz'][$questions_container_name]['questions_options'] = array(
      '#title' => check_plain($question->title),
      '#type' => 'fieldset',
      '#weight' => 2,
    );

    $quiz_questions_options = $question->field_options[LANGUAGE_NONE];
    $quiz_questions_opt_values = array();

    foreach ($quiz_questions_options as $key_opt => $question_option) {
      $quiz_questions_opt_values[$question_option['question_option_score']] = $question_option['question_option'];
    }

    $form['quiz'][$questions_container_name]['questions_options'][$question_name] = array(
      '#type' => 'radios',
      '#id' => $question_name . '_' . $key_opt,
      '#options' => $quiz_questions_opt_values,
      '#required' => TRUE,
    );

    $back_question = $key;
    $next_question = $key + 2;
    $button_markup = '<div class="wrap-buttons">'
      . '<a class="back-question enabled button-quiz" rel="' . $back_question
      . '">' . t('Back') . '</a><a class="next-question button-quiz" rel="'
      . $next_question . '">' . t('Next') . '</a></div>';
    $form['quiz'][$questions_container_name]['buttons'] = array(
      '#markup' => $button_markup,
      '#weight' => 3,
    );

    $illustration = field_get_items('node', $question, 'field_illustration');
    $illustration_file = file_create_url($illustration[0]['uri']);

    if ($illustration) {
      $illustration_markup = '<div class="illustration-wrapper"><img src="'
      . $illustration_file . '" class="illustration-image"/></div>';
      $form['quiz'][$questions_container_name]['illustration'] = array(
        '#markup' => $illustration_markup,
        '#weight' => 4,
      );
    }

    $form['quiz'][$questions_container_name]['#suffix'] = '</div>';
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#id' => 'flexible-quiz-submit',
    '#submit' => array('flexible_quiz_question_form_submit'),
  );

  return $form;
}


/**
 * Implements hook_form_validate().
 */
function flexible_quiz_question_form_validate($form, &$form_state) {
  foreach ($form_state['values'] as $opt_field => $value) {
    if (preg_match("/question_/", $opt_field)) {
      if (!$value) {
        form_set_error($opt_field);
        drupal_set_message(check_plain(t("Field") . " # " . $opt_field), 'error');
      }
    }
  }
}


/**
 * Implements hook_form_submit().
 */
function flexible_quiz_question_form_submit($form, &$form_state) {

  $quiz_nid = $form_state['values']['quiz_nid'];
  $quiz_questions_data = $form_state['values'];
  $field_key = array();
  $quiz_score = 0;

  foreach ($quiz_questions_data as $key => $score) {
    if (preg_match("/question_/", $key)) {
      $field_key[] = array(
        'nid' => str_replace("question_", "", $key),
        'score' => $score,
      );

      $quiz_score += $score;
    }
  }

  $quiz_result_page = _flexible_quiz_get_result_node($quiz_nid, $quiz_score);

  $quiz_result_submission = array('nid' => $quiz_result_page->entity_id, 'total_score' => $quiz_score);

  _flexible_quiz_store_results($quiz_nid, $field_key, $quiz_result_submission);

  $form_state['redirect'] = "flexible_quiz/result/" . $quiz_result_page->entity_id;
}

/**
 * Renders the flexible_quiz_result passed by parameter.
 */
function flexible_quiz_result($nid) {
  _flexible_quiz_add_js_and_css();
  $flexible_quiz_result = node_load($nid);

  $flexible_quiz_result_array = node_view($flexible_quiz_result);
  return drupal_render($flexible_quiz_result_array);
}

/**
 * Return an array with Flexible Quiz Questions nodes.
 */
function _flexible_quiz_questions($flexible_quiz_node) {

  $query = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', 'flexible_quiz_question', '=');

  $query->join('field_data_field_question_quiz', 'fqq', 'fqq.entity_id = n.nid');
  $query->condition('fqq.field_question_quiz_nid', $flexible_quiz_node, '=');

  $result = $query
    ->execute()
    ->fetchAll();

  $nodes = array();
  foreach ($result as $record) {
    $nodes[] = node_load($record->nid);
  }

  return $nodes;
}

/**
 * Return a flexible_quiz_result object according with Quiz ID and Score.
 */
function _flexible_quiz_get_result_node($quiz_nid, $quiz_score) {

  $query = db_select('field_revision_field_quiz_reference_result', 'fqrr')
    ->condition('fqrr.field_quiz_reference_result_nid', $quiz_nid, '=');

  $query->join('field_revision_field_result_range_start', 'fqrrs', 'fqrrs.entity_id = fqrr.entity_id');
  $query->condition('fqrrs.field_result_range_start_value', $quiz_score, '<=');

  $query->join('field_revision_field_result_range_end', 'fqre', 'fqre.entity_id = fqrr.entity_id');
  $query->condition('fqre.field_result_range_end_value', $quiz_score, '>=');

  $query->fields('fqrr');

  $result = $query
    ->execute()
    ->fetch();

  return $result;
}

/**
 * Storing Quiz submissions into the database.
 */
function _flexible_quiz_store_results($quiz_nid, $field_key, $quiz_result_submission) {
  $quiz_submission = new stdClass();
  $quiz_submission->nid = $quiz_nid;
  $quiz_submission->submitted = REQUEST_TIME;
  $quiz_submission->submission_data['questions'] = $field_key;
  $quiz_submission->submission_data['result'] = $quiz_result_submission;
  drupal_write_record('flexible_quiz_submissions', $quiz_submission);
}

/**
 * Add javascript and css needed.
 */
function _flexible_quiz_add_js_and_css() {
  drupal_add_css(drupal_get_path('module', 'flexible_quiz') . '/styles/flexible_quiz.css');
  drupal_add_js(drupal_get_path('module', 'flexible_quiz') . '/scripts/flexible_quiz.js');
  drupal_add_library('system', 'effects.slide');
}
