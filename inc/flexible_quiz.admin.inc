<?php
/**
 * @file
 * Flexible Quiz admin settings.
 */

/**
 * Form builder. Configure flexible quiz general settings.
 */
function flexible_quiz_general_settings($form, &$form_state) {
  $form = array();

  $form['flexible_quiz_landing_page_url'] = array(
    '#title'         => t('Landing Page URL'),
    '#description'   => t('Use % as parameter to reference you flexible_quiz node. e.g flexible_quiz/%'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('flexible_quiz_landing_page_url', 'flexible_quiz/%'),
  );

  $form['flexible_quiz_questions_page_url'] = array(
    '#title'         => t('Questions Page URL'),
    '#description'   => t('Use % as parameter to reference you flexible_quiz node. e.g flexible_quiz/questions/%'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('flexible_quiz_questions_page_url', 'flexible_quiz/questions/%'),
  );

  $form['flexible_quiz_result_page_url'] = array(
    '#title'         => t('Result Page URL'),
    '#description'   => t('Use % as parameter to reference you flexible_quiz node. e.g flexible_quiz/result/%'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('flexible_quiz_result_page_url', 'flexible_quiz/result/%'),
  );

  $form['#submit'][] = 'flexible_quiz_general_settings_submit';

  return system_settings_form($form);
}

/**
 * Forces all caches to be fluxed after submitting the settings form.
 */
function flexible_quiz_general_settings_submit() {
  drupal_flush_all_caches();
}
