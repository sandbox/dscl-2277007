/**
 * @file
 * Apply Flexible Quiz scripts.
 */

 (function ($) {

  $(document).ready(function() {
    initializePromoBlockButton();
    initializeQuestionsPage();
    handleNextButton();
    handleBackButton();
    handleOptions();
    handleSubmitButton();
  });

  function initializePromoBlockButton() {
    $('a.redirect-first-question').click(function(){
      var url = $(this).attr('rel');
      $('<form action="' + url + '" method="POST"></form>').submit();
    });
  }

  function initializeQuestionsPage() {
    if ($('.question-wrapper').size() != 1) {
      $('.question-wrapper').hide();
      $('.question-wrapper').first().show();
    }
  }

  function handleNextButton() {
    $('.question-wrapper').last().find('.next-question').remove();

    $('.next-question').live('click', function() {
      var id = $(this).attr('rel');
      var question_selector = '.question-' + id;

      if (checkSelectedValue(id - 1)) {
        $('.question-wrapper').hide();
        $(question_selector).effect('slide', { direction: 'right', mode: 'show' }, 500);
      }
      else {
        var msgError = Drupal.t("Select a option for '") + $(".question-" + (id - 1)).find("legend span").text() + "'";
        alert(msgError);
        return false;
      }
    });
  }

  function handleBackButton() {
    $('.question-wrapper').first().find('.back-question').remove();

    $('.back-question').click(function() {

      var id = $(this).attr('rel');
      var question_selector = '.question-' + id;

      $('.question-wrapper').hide();
      $(question_selector).effect('slide', { direction: 'left', mode: 'show' }, 500);

    });
  }

  function handleOptions() {
    $('.question-wrapper:visible input').change(function() {
      if ($('.question-wrapper:visible input:checked')) {
        $('.question-wrapper:visible .next-question').addClass('enabled');
      }
    });
  }

  function handleSubmitButton() {
    $('#flexible-quiz-submit').appendTo($('.question-wrapper').last().find('.wrap-buttons'));
    $('#flexible-quiz-question-form').live('submit', function() {
      var id = $('.question-wrapper').size();
      var question_selector = '.question-' + id;

      if (!checkSelectedValue(id)) {
        var msgError = "Select a option for '" + $(question_selector).find("legend span").text() + "'";
        alert(msgError);
        return false;
      }
    });
  }

  function checkSelectedValue(qid) {
    var q_option = $(".question-" + qid + " input[type='radio']:checked");
    var optSelected = false;

    if (q_option.size() >= 1) {
      optSelected = true;
    }

    return optSelected;
  }

})(jQuery);
