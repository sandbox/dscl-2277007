How To UNINSTALL module

* Remove 'Question Options' field on 'Flexible Quiz Question' content type
  - Access admin/structure/types/manage/flexible_quiz_question/fields
  - Click on 'delete' of 'field_options'

* Run cron to make Drupal fields deletion

* Disable an uninstall as usual.

================================================================================
