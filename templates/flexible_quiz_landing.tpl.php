<?php
/**
 * @file
 * Displays the landing page for the flexible_quiz content type.
 *
 * Available variables:
 * - $node: The node object for the quiz being displayed.
 *
 * @see flexible_quiz_theme()
 */

$background = field_get_items('node', $node, 'field_quiz_bkg_image');
$background_file = file_create_url($background[0]['uri']);
?>
<div id="node-<?php print $node->nid; ?>" class="quiz-landing"
     style="background: url('<?php print $background_file; ?>') repeat-x;">
  <div class="content">
    <h3>
    <?php
      $subtitle = field_get_items('node', $node, 'field_subtitle');
      print $subtitle[0]['value'];
    ?>
    </h3>
    <h2><?php print check_plain($node->title); ?></h2>
    <div class="description">
    <?php
      $description = field_get_items('node', $node, 'body');
      print $description[0]['value'];
    ?>
    </div>

    <div class="wrap-button">
      <a class="start-quiz redirect-first-question button-quiz" rel="<?php global $base_url; print $base_url . "/" . $node->start_quiz_link; ?>">
        <?php print t('Start the Quiz'); ?>
      </a>
    </div>

    <?php
      $illustration = field_get_items('node', $node, 'field_quiz_image');
      $illustration_file = file_create_url($illustration[0]['uri']);
    ?>
    <?php if ($illustration) : ?>
      <div class="illustration-wrapper">
          <img src="<?php print $illustration_file?>" class="illustration-image"/>
      </div>
    <?php endif; ?>
  </div>
</div>
