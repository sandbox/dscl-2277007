<?php
/**
 * @file
 * Displays a question for a quiz.
 *
 * Available variables:
 * - $node: The node object for the question being displayed.
 */

$background = field_get_items('node', $node, 'field_bkg_image');
$background_file = file_create_url($background[0]['uri']);
?>
<div id="node-<?php print $node->nid; ?>" class="question-wrapper"
     style="background: url('<?php print $background_file; ?>') repeat-x;">

  <div class="question-text"><?php print check_plain($node->title); ?></div>

  <div class="question-options">
    <?php $options = field_get_items('node', $node, 'field_options'); ?>
    <?php foreach ($options as $i => $option): ?>
      <input type="radio" value="<?php print $option['question_option_score']?>" name="question" />
      <span class="option"><?php print $option['question_option']?></span>
    <?php endforeach; ?>
  </div>

  <?php
    $illustration = field_get_items('node', $node, 'field_illustration');
    $illustration_file = file_create_url($illustration[0]['uri']);
  ?>
  <?php if($illustration) : ?>
    <div class="illustration-wrapper">
      <img src="<?php print $illustration_file?>" class="illustration-image"/>
    </div>
  <?php endif; ?>
</div>
