<?php
/**
 * @file
 * Displays a result for a quiz.
 *
 * Available variables:
 * - $node: The node object for the quiz result being displayed.
 */

$background = field_get_items('node', $node, 'field_result_persona_bkg_image');
$background_file = file_create_url($background[0]['uri']);
?>
<div class="result-page" style="background: url('<?php print $background_file; ?>') repeat;">
  <div class="wrap-result-text">
    <h1><?php print check_plain($node->title) ?></h1>
    <div class="subtitle">
    <?php
      $output = field_view_field('node', $node, 'field_result_subtitle', array('label' => 'hidden'));
      print render($output);
    ?>
    </div>
    <div class="description">
    <?php
      $output = field_view_field('node', $node, 'body', array('label' => 'hidden'));
      print render($output);
    ?>
    </div>
    <div class="button">
      <?php
        $reference = field_get_items('node', $node, 'field_quiz_reference_result');
        $quiz_nid = $reference[0]['nid'];

        $quiz_url = variable_get('flexible_quiz_landing_page_url', 'flexible_quiz/%');
        $quiz_url = str_replace('%', $quiz_nid, $quiz_url);
      ?>
      <a href="/<?php print $quiz_url; ?>" title="<?php print t('Retake the Quiz'); ?>"><?php print t('Retake the Quiz'); ?></a>
    </div>
  </div>
  <div class="wrap-result-image">
  <?php
    $output = field_view_field('node', $node, 'field_result_persona_image', array('label' => 'hidden'));
    print render($output);
  ?>
  </div>
</div>
