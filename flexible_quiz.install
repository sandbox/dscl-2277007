<?php

/**
 * @file
 * Install file for Flexible Quiz module.
 */

define('QUIZ_NODE_TYPE', 'flexible_quiz');
define('QUESTION_NODE_TYPE', 'flexible_quiz_question');
define('RESULT_NODE_TYPE', 'flexible_quiz_result');
define('COMMENT_NODE_DISABLED', 0);

/**
 * Implements hook_install().
 */
function flexible_quiz_install() {
  node_types_rebuild();
  $available_node_types = node_type_get_types();

  node_add_body_field($available_node_types[QUIZ_NODE_TYPE], 'Description');
  flexible_quiz_add_quiz_content_type_fields();

  flexible_quiz_add_question_content_type_fields();

  node_add_body_field($available_node_types[RESULT_NODE_TYPE]);
  flexible_quiz_add_result_content_type_fields();

  flexible_quiz_disable_comments();
}

/**
 * Implements hook_uninstall().
 */
function flexible_quiz_uninstall() {
  flexible_quiz_remove_quiz_references();
  flexible_quiz_remove_question_references();
  flexible_quiz_remove_result_references();

  flexible_quiz_remove_variables();

  cache_clear_all();
}

/**
 * Implements hook_schema().
 */
function flexible_quiz_schema() {
  $schema['flexible_quiz_submissions'] = array(

    // Implementing the custom table to store the quiz submissions.
    'description' => 'Custom table to store the quiz submissions.',
    'fields' => array(
      'sid' => array(
        'description' => 'The Quiz submission identifier.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'nid' => array(
        'description' => 'The Quiz node ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'vid' => array(
        'description' => 'The current Quiz Content {node_revision}.vid version identifier.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'uid' => array(
        'description' => 'User ID for the related submission',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'submitted' => array(
        'description' => 'Quiz submission date.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'submission_data' => array(
        'description' => 'Quiz questions, answers and result match for this submission.',
        'type' => 'text',
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
    ),
    'indexes' => array(
      'flexible_quiz_submission_submitted' => array('submitted'),
      'flexible_quiz_submission_nid' => array('nid'),
    ),
    'foreign keys' => array(
      'flexible_quiz_nid' => array(
        'table' => 'node',
        'columns' => array('nid' => 'nid'),
      ),
      'flexible_quiz_revision' => array(
        'table' => 'node_revision',
        'columns' => array('vid' => 'vid'),
      ),
    ),
    'primary key' => array('sid'),
  );

  return $schema;
}

/**
 * Creates the fields for the quiz, content type.
 */
function flexible_quiz_add_quiz_content_type_fields() {
  $fields = flexible_quiz_get_quiz_content_type_fields_definition();

  foreach ($fields as $field) {
    field_create_field($field);
    field_create_instance($field);
  }
}


/**
 * Creates the fields for the question, content type.
 */
function flexible_quiz_add_question_content_type_fields() {
  $fields = flexible_quiz_get_question_content_type_fields_definition();

  foreach ($fields as $field) {
    field_create_field($field);
    field_create_instance($field);
  }
}

/**
 * Creates the fields for the result, content type.
 */
function flexible_quiz_add_result_content_type_fields() {

  $fields = _flexible_quiz_get_result_content_type_fields_definition();

  foreach ($fields as $field) {
    field_create_field($field);
    field_create_instance($field);
  }
}

/**
 * Implements hook_field_schema().
 */
function flexible_quiz_field_schema($field) {
  if ($field['type'] == 'flexible_quiz_options') {
    $varchar = array(
      'type'     => 'varchar',
      'length'   => 500,
      'not null' => FALSE,
    );

    $int = array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => FALSE,
    );

    $columns = array(
      'question_option' => $varchar,
      'question_option_score' => $int,
    );

    return array(
      'columns' => $columns,
      'indexes' => array(),
    );
  }
}

/**
 * Flexible Quiz fields definition.
 */
function flexible_quiz_get_quiz_content_type_fields_definition() {

  $fields = array(
    array(
      'field_name' => 'field_subtitle',
      'type' => 'text',
      'translatable' => FALSE,
      'entity_type' => 'node',
      'bundle' => QUIZ_NODE_TYPE,
      'label' => t('Subtitle'),
      'cardinality' => 1,
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    array(
      'field_name' => 'field_quiz_bkg_image',
      'type' => 'image',
      'translatable' => FALSE,
      'entity_type' => 'node',
      'bundle' => QUIZ_NODE_TYPE,
      'label' => t('Background Image'),
      'cardinality' => 1,
      'module' => 'image',
      'widget' => array(
        'type' => 'image_image',
        'weight' => 10,
      ),
      'formatter' => array(
        'label' => t('Background Image'),
        'format' => 'image',
      ),
      'settings' => array(
        'file_directory' => 'quiz',
        'max_filesize' => '4M',
        'preview_image_style' => 'thumbnail',
        'title_field' => FALSE,
        'alt_field' => FALSE,
      ),
    ),
    array(
      'field_name' => 'field_quiz_image',
      'type' => 'image',
      'translatable' => FALSE,
      'entity_type' => 'node',
      'bundle' => QUIZ_NODE_TYPE,
      'label' => t('Image'),
      'cardinality' => 1,
      'module' => 'image',
      'widget' => array(
        'type' => 'image_image',
        'weight' => 10,
      ),
      'formatter' => array(
        'label' => t('Image'),
        'format' => 'image',
      ),
      'settings' => array(
        'file_directory' => 'quiz',
        'max_filesize' => '4M',
        'preview_image_style' => 'thumbnail',
        'title_field' => FALSE,
        'alt_field' => FALSE,
      ),
    ),
  );

  return $fields;
}

/**
 * Flexible Quiz Question fields definition.
 */
function _flexible_quiz_get_question_content_type_fields_definition() {

  $fields = array(
    array(
      'field_name' => 'field_bkg_image',
      'type' => 'image',
      'translatable' => FALSE,
      'entity_type' => 'node',
      'bundle' => QUESTION_NODE_TYPE,
      'label' => t('Background Image'),
      'cardinality' => 1,
      'module' => 'image',
      'widget' => array(
        'type' => 'image_image',
        'weight' => 10,
      ),
      'formatter' => array(
        'label' => t('Background Image'),
        'format' => 'image',
      ),
      'settings' => array(
        'file_directory' => 'quiz',
        'max_filesize' => '4M',
        'preview_image_style' => 'thumbnail',
        'title_field' => FALSE,
        'alt_field' => FALSE,
      ),
    ),
    array(
      'field_name' => 'field_illustration',
      'type' => 'image',
      'translatable' => FALSE,
      'entity_type' => 'node',
      'bundle' => QUESTION_NODE_TYPE,
      'label' => t('Illustration'),
      'cardinality' => 1,
      'module' => 'image',
      'widget' => array(
        'type' => 'image_image',
        'weight' => 10,
      ),
      'formatter' => array(
        'label' => t('Illustration'),
        'format' => 'image',
      ),
      'settings' => array(
        'file_directory' => 'quiz',
        'max_filesize' => '4M',
        'preview_image_style' => 'thumbnail',
        'title_field' => FALSE,
        'alt_field' => FALSE,
      ),
    ),
    array(
      'field_name' => 'field_options',
      'type' => 'flexible_quiz_options',
      'translatable' => TRUE,
      'entity_type' => 'node',
      'bundle' => QUESTION_NODE_TYPE,
      'label' => t('Question Options'),
      'cardinality' => -1,
    ),
    array(
      'field_name' => 'field_question_quiz',
      'label' => 'Quiz',
      'entity_type' => 'node',
      'module' => 'node_reference',
      'bundle' => QUESTION_NODE_TYPE,
      'translatable' => FALSE,
      'type' => 'node_reference',
      'settings' => array(
        'referenceable_types' => array(
          'flexible_quiz' => 'flexible_quiz',
        ),
      ),
      'cardinality' => 1,
      'widget' => array(
        'type' => 'nodereference_url',
        'settings' => array(
          'fallback' => 'select',
          'edit_fallback' => 1,
          'node_link' => array(
            'destination' => 'node',
          ),
          'referenceable_types' => array(
            'flexible_quiz' => 'flexible_quiz',
            'flexible_quiz_question' => 'flexible_quiz_question',
          ),
        ),
      ),
    ),
  );

  return $fields;
}

/**
 * Flexible Quiz Result fields definition.
 */
function _flexible_quiz_get_result_content_type_fields_definition() {

  $fields = array(
    array(
      'field_name' => 'field_result_subtitle',
      'type' => 'text',
      'translatable' => TRUE,
      'entity_type' => 'node',
      'bundle' => RESULT_NODE_TYPE,
      'label' => t('Subtitle'),
    ),
    array(
      'field_name' => 'field_result_persona_bkg_image',
      'type' => 'image',
      'translatable' => FALSE,
      'entity_type' => 'node',
      'bundle' => RESULT_NODE_TYPE,
      'label' => t('Persona Background Image'),
      'cardinality' => 1,
      'module' => 'image',
      'widget' => array(
        'type' => 'image_image',
        'weight' => 10,
      ),
      'formatter' => array(
        'label' => t('Persona Background Image'),
        'format' => 'image',
      ),
      'settings' => array(
        'file_directory' => 'quiz',
        'max_filesize' => '4M',
        'preview_image_style' => 'thumbnail',
        'title_field' => FALSE,
        'alt_field' => FALSE,
      ),
    ),
    array(
      'field_name' => 'field_result_persona_image',
      'type' => 'image',
      'translatable' => FALSE,
      'entity_type' => 'node',
      'bundle' => RESULT_NODE_TYPE,
      'label' => t('Persona Image'),
      'cardinality' => 1,
      'module' => 'image',
      'widget' => array(
        'type' => 'image_image',
        'weight' => 10,
      ),
      'formatter' => array(
        'label' => t('Persona Image'),
        'format' => 'image',
      ),
      'settings' => array(
        'file_directory' => 'quiz',
        'max_filesize' => '4M',
        'preview_image_style' => 'thumbnail',
        'title_field' => FALSE,
        'alt_field' => FALSE,
      ),
    ),
    array(
      'field_name' => 'field_quiz_reference_result',
      'label' => 'Quiz',
      'entity_type' => 'node',
      'module' => 'node_reference',
      'bundle' => RESULT_NODE_TYPE,
      'translatable' => FALSE,
      'type' => 'node_reference',
      'settings' => array(
        'referenceable_types' => array(
          'flexible_quiz' => 'flexible_quiz',
        ),
      ),
      'cardinality' => 1,
      'widget' => array(
        'type' => 'nodereference_url',
        'settings' => array(
          'fallback' => 'select',
          'edit_fallback' => 1,
          'node_link' => array(
            'destination' => 'node',
          ),
          'referenceable_types' => array(
            'flexible_quiz' => 'flexible_quiz',
            'flexible_quiz_question' => 'flexible_quiz_question',
          ),
        ),
      ),
    ),
    array(
      'field_name' => 'field_result_range_start',
      'type' => 'number_integer',
      'translatable' => TRUE,
      'entity_type' => 'node',
      'bundle' => RESULT_NODE_TYPE,
      'label' => t('Range Start'),
    ),
    array(
      'field_name' => 'field_result_range_end',
      'type' => 'number_integer',
      'translatable' => TRUE,
      'entity_type' => 'node',
      'bundle' => RESULT_NODE_TYPE,
      'label' => t('Range End'),
    ),
  );

  return $fields;
}

/**
 * Removes all references to the quiz content type.
 */
function flexible_quiz_remove_quiz_references() {
  flexible_quiz_remove_contents(QUIZ_NODE_TYPE);
  flexible_quiz_delete_quiz_content_type_fields();
}

/**
 * Removes all references to the question content type.
 */
function flexible_quiz_remove_question_references() {
  flexible_quiz_remove_contents(QUESTION_NODE_TYPE);
  flexible_quiz_delete_question_content_type_fields();
}

/**
 * Removes all references to the result content type.
 */
function flexible_quiz_remove_result_references() {
  flexible_quiz_remove_contents(RESULT_NODE_TYPE);
  flexible_quiz_delete_result_content_type_fields();
}

/**
 * Remove nodes by content type then remove the content type.
 */
function flexible_quiz_remove_contents($content_type) {
  $sql = 'SELECT nid FROM {node} n WHERE n.type = :type';
  $result = db_query($sql, array(':type' => $content_type));
  $nids = array();
  foreach ($result as $row) {
    $nids[] = $row->nid;
  }
  node_delete_multiple($nids);
  node_type_delete($content_type);
}

/**
 * Delete Flexible Quiz content type fields.
 */
function flexible_quiz_delete_quiz_content_type_fields() {
  $fields = flexible_quiz_get_quiz_content_type_fields_definition();

  foreach ($fields as $field) {
    field_delete_instance($field);
    field_delete_field($field['field_name']);
  }
}


/**
 * Delete Flexible Quiz Question content type fields.
 */
function flexible_quiz_delete_question_content_type_fields() {
  $fields = flexible_quiz_get_question_content_type_fields_definition();

  foreach ($fields as $field) {
    field_delete_instance($field);
    field_delete_field($field['field_name']);
  }
}

/**
 * Delete Flexible Quiz Result content type fields.
 */
function flexible_quiz_delete_result_content_type_fields() {
  $fields = flexible_quiz_get_result_content_type_fields_definition();

  foreach ($fields as $field) {
    field_delete_instance($field);
    field_delete_field($field['field_name']);
  }
}

/**
 * Disable comments for created content types.
 */
function flexible_quiz_disable_comments() {
  variable_set("node_options_" . QUIZ_NODE_TYPE, array('status'));
  variable_set('comment_' . QUIZ_NODE_TYPE, COMMENT_NODE_DISABLED);

  variable_set("node_options_" . QUESTION_NODE_TYPE, array('status'));
  variable_set('comment_' . QUESTION_NODE_TYPE, COMMENT_NODE_DISABLED);

  variable_set("node_options_" . RESULT_NODE_TYPE, array('status'));
  variable_set('comment_' . RESULT_NODE_TYPE, COMMENT_NODE_DISABLED);
}

/**
 * Remove variables defined by this module.
 */
function flexible_quiz_remove_variables() {
  variable_del("node_options_" . QUIZ_NODE_TYPE);
  variable_del('comment_' . QUIZ_NODE_TYPE);

  variable_del("node_options_" . QUESTION_NODE_TYPE);
  variable_del('comment_' . QUESTION_NODE_TYPE);

  variable_del("node_options_" . RESULT_NODE_TYPE);
  variable_del('comment_' . RESULT_NODE_TYPE);
}
